package br.com.mastertech.porta.controller;

import br.com.mastertech.porta.model.Porta;
import br.com.mastertech.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta createPorta(@RequestBody Porta porta){
        return portaService.create(porta);
    }


    @GetMapping("/{id}")
    public Porta getPortaById(@PathVariable(name = "id") Long id){
        return portaService.findById(id);
    }
}
