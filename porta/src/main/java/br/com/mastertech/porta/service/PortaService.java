package br.com.mastertech.porta.service;

import br.com.mastertech.porta.exceptions.PortaNotFoundException;
import br.com.mastertech.porta.model.Porta;
import br.com.mastertech.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta create(Porta porta) {
        return portaRepository.save(porta);
    }

    public Porta findById(long id){
        Optional<Porta> porta = portaRepository.findById(id);

        if (!porta.isPresent()){
            throw new PortaNotFoundException();
        }

        return porta.get();
    }
}
