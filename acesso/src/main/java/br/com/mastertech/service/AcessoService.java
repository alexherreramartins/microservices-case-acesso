package br.com.mastertech.service;

import br.com.mastertech.clients.cliente.Cliente;
import br.com.mastertech.clients.cliente.ClienteClient;
import br.com.mastertech.clients.porta.Porta;
import br.com.mastertech.clients.porta.PortaClient;
import br.com.mastertech.exceptions.AcessoNotFoundException;
import br.com.mastertech.model.Acesso;
import br.com.mastertech.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private LogProducer producer;

    public Acesso create(Acesso acesso) {

        Cliente cliente = clienteClient.getById(acesso.getClienteId());
        Porta porta = portaClient.getById(acesso.getPortaId());

        return acessoRepository.save(acesso);
    }

    public Acesso findById(long id){
        Optional<Acesso> acesso = acessoRepository.findById(id);

        if (!acesso.isPresent()){
            throw new AcessoNotFoundException();
        }
        return acesso.get();
    }

    public Acesso findByPortaIdAndClienteId(long cliente, long porta){
        Optional<Acesso> acesso = acessoRepository.findByClienteIdAndPortaId(cliente,porta);

        if (!acesso.isPresent()){
            producer.enviarAoKafka("Cliente " + cliente + ", sem acesso a porta: "+ porta);
            throw new AcessoNotFoundException();
        }
        producer.enviarAoKafka("Cliente " + cliente + ", com acesso a porta: "+ porta);
        return acesso.get();
    }

    public void delete(long clienteId, long portaId) {

        Acesso acesso = findByPortaIdAndClienteId(clienteId,portaId);

        acessoRepository.delete(acesso);
    }


}
