package br.com.mastertech.service;

import br.com.mastertech.producer.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class LogProducer {

    @Autowired
    private KafkaTemplate<String, Log> producer;

    public void enviarAoKafka(String message) {
        Log log = new Log();
        log.setMessage(message);
        log.setServiceName("microservice - Acesso");

        producer.send("spec4-alex-herrera-2", log);
    }

}
