package br.com.mastertech.controller;

import br.com.mastertech.dtos.AcessoMapper;
import br.com.mastertech.dtos.AcessoDataTrasferObject;
import br.com.mastertech.model.Acesso;
import br.com.mastertech.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper acessoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoDataTrasferObject createAcesso(@RequestBody AcessoDataTrasferObject createAcessoRequest){

        Acesso acesso = acessoService.create(acessoMapper.toAcesso(createAcessoRequest));

        return acessoMapper.toAcessoDataTrasferObject(acesso);
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAcesso(@PathVariable(name = "cliente_id") long clienteId, @PathVariable(name = "porta_id") long portaId ){

        acessoService.delete(clienteId,portaId);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public AcessoDataTrasferObject getAcesso(@PathVariable(name = "cliente_id") long clienteId, @PathVariable(name = "porta_id") long portaId ){

       Acesso acesso =  acessoService.findByPortaIdAndClienteId(clienteId,portaId);

       return acessoMapper.toAcessoDataTrasferObject(acesso);
    }


}
