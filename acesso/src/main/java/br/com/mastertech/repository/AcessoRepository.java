package br.com.mastertech.repository;

import br.com.mastertech.model.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    Optional<Acesso> findByClienteIdAndPortaId(long clienteId, long portaId);
}
