package br.com.mastertech.clients.porta;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/porta/{id}")
    Porta getById(@PathVariable long id);
}
