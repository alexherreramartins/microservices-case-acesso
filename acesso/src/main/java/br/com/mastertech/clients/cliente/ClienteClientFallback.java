package br.com.mastertech.clients.cliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente getById(long id) {
       throw new ResponseStatusException(HttpStatus.GATEWAY_TIMEOUT, "Microserviço de clientes fora!");
    }
}
