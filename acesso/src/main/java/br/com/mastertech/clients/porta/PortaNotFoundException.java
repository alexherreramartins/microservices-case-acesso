package br.com.mastertech.clients.porta;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Porta não cadastrado")
public class PortaNotFoundException extends RuntimeException {

}
