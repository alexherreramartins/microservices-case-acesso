package br.com.mastertech.clients.porta;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta getById(long id) {
       throw new ResponseStatusException(HttpStatus.GATEWAY_TIMEOUT, "Microserviço de porta fora!");
    }
}
