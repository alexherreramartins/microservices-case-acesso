package br.com.mastertech.clients.cliente;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder getClienteClientDecoder(){
        return new ClienteClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new ClienteClientFallback(), RuntimeException.class)
                .build();
        return Resilience4jFeign.builder(feignDecorators);
    }

}
