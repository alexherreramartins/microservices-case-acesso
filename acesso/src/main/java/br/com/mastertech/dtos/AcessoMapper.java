package br.com.mastertech.dtos;

import br.com.mastertech.model.Acesso;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {
    public Acesso toAcesso(AcessoDataTrasferObject acessoDataTrasferObject) {
        Acesso acesso = new Acesso();

        acesso.setPortaId(acessoDataTrasferObject.getPorta_id());
        acesso.setClienteId(acessoDataTrasferObject.getCliente_id());

        return acesso;
    }

    public AcessoDataTrasferObject toAcessoDataTrasferObject(Acesso acesso) {
        AcessoDataTrasferObject acessoDataTrasferObject = new AcessoDataTrasferObject();

        acessoDataTrasferObject.setCliente_id(acesso.getClienteId());
        acessoDataTrasferObject.setPorta_id(acesso.getPortaId());

        return acessoDataTrasferObject;
    }
}
