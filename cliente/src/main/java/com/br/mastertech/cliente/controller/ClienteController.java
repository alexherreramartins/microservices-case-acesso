package com.br.mastertech.cliente.controller;

import com.br.mastertech.cliente.model.Cliente;
import com.br.mastertech.cliente.model.ClienteMapper;
import com.br.mastertech.cliente.model.dtos.ClienteRequest;
import com.br.mastertech.cliente.model.dtos.ClienteResponse;
import com.br.mastertech.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteResponse createCliente(@RequestBody ClienteRequest clienteRequest ){

        Cliente cliente = clienteService.createCliente(clienteMapper.toCliente(clienteRequest));

        return clienteMapper.toClienteResponse(cliente);
    }

    @GetMapping("/{id}")
    public ClienteResponse getCliente(@PathVariable(name = "id") int id ){

        Cliente cliente = clienteService.findClienteById(id);

        return clienteMapper.toClienteResponse(cliente);
    }
}
