package com.br.mastertech.cliente.model;

import com.br.mastertech.cliente.model.dtos.ClienteRequest;
import com.br.mastertech.cliente.model.dtos.ClienteResponse;
import org.checkerframework.checker.units.qual.C;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente( ClienteRequest clienteRequest){
        Cliente cliente = new Cliente();

        cliente.setName(clienteRequest.getNome());

        return cliente;
    }

    public ClienteResponse toClienteResponse(Cliente cliente){
        ClienteResponse clienteResponse = new ClienteResponse();

        clienteResponse.setId(cliente.getId());
        clienteResponse.setNome(cliente.getName());

        return clienteResponse;
    }
}
