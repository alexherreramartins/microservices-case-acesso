package com.br.mastertech.cliente.repository;

import com.br.mastertech.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Integer> {
}
