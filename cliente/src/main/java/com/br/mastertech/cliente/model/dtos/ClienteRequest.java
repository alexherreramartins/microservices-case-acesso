package com.br.mastertech.cliente.model.dtos;

public class ClienteRequest {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
